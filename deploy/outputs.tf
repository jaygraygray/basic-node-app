output "db_host" {
  value = aws_db_instance.main.address
}

output "bastion_host" {
  value = aws_instance.bastion.public_dns
}

output "api_endpoint" {
  value = aws_lb.api.dns_name
}

output "api_endpoint_2" {
  value = aws_route53_record.app.fqdn
}

# output "second_api_endpoint" {
#   value = aws_route53_record.second_app.fqdn
# }