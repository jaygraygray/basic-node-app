terraform {
  backend "s3" {
    bucket         = "devops-tf-state00"
    key            = "boilerplate.tfstate"
    region         = "us-east-1"
    encrypt        = true
    dynamodb_table = "devops-tf-state00"
  }
}

provider "aws" {
  region  = "us-east-1"
  version = "~> 2.54.0"
}

locals {
  prefix = "${var.prefix}-${terraform.workspace}"
  common_tags = {
    Environment = terraform.workspace
    Project     = var.project
    Owner       = var.contact
    ManagedBy   = "Terraform"
  }
}

data "aws_region" "current" {}