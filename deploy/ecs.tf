resource "aws_ecs_cluster" "main" {
  name = "${local.prefix}-cluster"

  tags = local.common_tags
}

# create policy to grant ECS the permission to start a new task
resource "aws_iam_policy" "task_execution_role_policy" {
  name        = "${local.prefix}-task-exec-role-policy"
  path        = "/"
  description = "Allow retrieving of images and adding to logs"
  policy      = file("./templates/ecs/task-exec-role.json")
}

# create a role, and attach above policy to it
resource "aws_iam_role" "task_execution_role" {
  name               = "${local.prefix}-task-exec-role"
  assume_role_policy = file("./templates/ecs/assume-role-policy.json")

  tags = local.common_tags
}

# execute task
resource "aws_iam_role_policy_attachment" "task_execution_role" {
  role       = aws_iam_role.task_execution_role.name
  policy_arn = aws_iam_policy.task_execution_role_policy.arn
}

# giving permissions to tasks it needs at run time.
resource "aws_iam_role" "app_iam_role" {
  name               = "${local.prefix}-api-task"
  assume_role_policy = file("./templates/ecs/assume-role-policy.json")

  tags = local.common_tags
}

resource "aws_cloudwatch_log_group" "ecs_task_logs" {
  name = "${local.prefix}-api"

  tags = local.common_tags
}

data "template_file" "api_container_definitions" {
  template = file("./templates/ecs/container-definitions.json.tpl")

  vars = {
    app_image        = var.ecr_image_api
    proxy_image      = var.ecr_image_proxy
    db_host          = aws_db_instance.main.address
    db_name          = aws_db_instance.main.name
    db_user          = aws_db_instance.main.username
    db_pass          = aws_db_instance.main.password
    log_group_name   = aws_cloudwatch_log_group.ecs_task_logs.name
    log_group_region = data.aws_region.current.name
    allowed_hosts    = aws_lb.api.dns_name
  }
}

# create task definition
resource "aws_ecs_task_definition" "api" {
  # same as name
  family = "${local.prefix}-api"
  # passed in all values of variables
  container_definitions    = data.template_file.api_container_definitions.rendered
  requires_compatibilities = ["FARGATE"]
  # use our network
  network_mode = "awsvpc"
  # affects cost and performance
  cpu    = 512
  memory = 1024
  # gives permissions to start new container
  execution_role_arn = aws_iam_role.task_execution_role.arn
  # gives permissions for task that are required at runtime
  task_role_arn = aws_iam_role.app_iam_role.arn

  volume {
    name = "static"
  }

  tags = local.common_tags
}

# Limit inbound and outbound network connectivity
resource "aws_security_group" "ecs_service" {
  description = "Access for the ECS service"
  name        = "${local.prefix}-ecs-service"
  vpc_id      = aws_vpc.main.id

  egress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port = 5432
    to_port   = 5432
    protocol  = "tcp"
    cidr_blocks = [
      aws_subnet.private_a.cidr_block,
      aws_subnet.private_b.cidr_block,
    ]
  }

  # Port of the proxy 
  ingress {
    from_port = 8000
    to_port   = 8000
    protocol  = "tcp"
    security_groups = [
      aws_security_group.lb.id
    ]
  }

  tags = local.common_tags
}

resource "aws_ecs_service" "api" {
  name            = "${local.prefix}-api"
  cluster         = aws_ecs_cluster.main.name
  task_definition = aws_ecs_task_definition.api.family
  desired_count   = 1
  launch_type     = "FARGATE"

  network_configuration {
    subnets = [
      aws_subnet.private_a.id,
      aws_subnet.private_b.id,
    ]
    security_groups = [aws_security_group.ecs_service.id]
  }

  load_balancer {
    target_group_arn = aws_lb_target_group.api.arn
    container_name   = "proxy"
    container_port   = 8000
  }

  deployment_controller {
    type = "CODE_DEPLOY"
  }

  depends_on = [aws_lb_listener.api_https]
}

# resource "aws_ecs_service" "second-api" {
#   name            = "${local.prefix}-second-api"
#   cluster         = aws_ecs_cluster.main.name
#   task_definition = aws_ecs_task_definition.second-api.family
#   desired_count   = 1
#   launch_type     = "FARGATE"

#   network_configuration {
#     subnets = [
#       aws_subnet.private_a.id,
#       aws_subnet.private_b.id,
#     ]
#     security_groups = [aws_security_group.ecs_service.id]
#   }

#   load_balancer {
#     target_group_arn = aws_lb_target_group.second-api.arn
#     container_name   = "proxy"
#     container_port   = 8000
#   }

#   depends_on = [aws_lb_listener.api_https]
# }