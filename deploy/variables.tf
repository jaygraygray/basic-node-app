variable "prefix" {
  default = "test"
}

variable "project" {
  default = "api-devops"
}

variable "contact" {
  default = "jaygray"
}

variable "db_username" {
  description = "User name for RDS postgres instance"
  default     = ""
}

variable "db_password" {
  description = "Password for the RDS postgres instance"
  default     = ""
}

variable "bastion_key_name" {
  default = "nodeapp-devops-bastion"
}

variable "ecr_image_api" {
  description = "ECR image for API"
  default     = "703936373391.dkr.ecr.us-east-1.amazonaws.com/node-test-app:latest"
}

variable "ecr_image_proxy" {
  description = "ECR image for proxy"
  default     = "703936373391.dkr.ecr.us-east-1.amazonaws.com/api-proxy:latest"
}

variable "dns_zone_name" {
  description = "Domain name"
  default     = "jaygray.gratis"
}

variable "subdomain" {
  description = "Subdomain per environment"
  type        = map(string)
  default = {
    production = "api"
    staging    = "api.staging"
    dev        = "api.dev"
  }
}

variable "second_api_subdomain" {
  description = "Subdomain per environment for second API service"
  type        = map(string)
  default = {
    production = "api2"
    staging    = "api2.staging"
    dev        = "api2.dev"
  }
}